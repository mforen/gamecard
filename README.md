# Jeu de Cartes - Projet Symfony

Ce projet est un jeu de cartes développé avec Symfony. Le jeu consiste à tirer une main de 10 cartes de manière aléatoire et à les afficher, puis à afficher la main triée par couleur et valeur.

## Structure du Projet

- Le dossier `src` contient les classes principales du projet.

  - `Controller` : Contient les contrôleurs gérant les interactions avec l'utilisateur.
  - `Service` : Contient la logique métier, comme le tirage et le tri des cartes.
  - `Entity` : Contient la classe `Card` qui représente une carte.

- Le dossier `templates` contient les fichiers Twig pour l'interface graphique.

  - `card` : Contient les fichiers Twig pour les pages de jeu.

- Le dossier `bin`, `config`, `public`, etc. sont des dossiers typiques d'une application Symfony.

## Choix de Classes

- `Card` (dans `Entity`) : Cette classe représente une carte avec sa couleur et sa valeur. Elle est utilisée pour créer les objets de carte dans le jeu.

- `CardGame` (dans `Service`) : Cette classe gère la logique du jeu. Elle permet de tirer une main aléatoire de 10 cartes et de les trier par couleur et valeur.

- `CardController` (dans `Controller`) : Ce contrôleur gère les interactions avec l'utilisateur. Il affiche les mains triées et non triées en mode graphique.

## Comment Exécuter et Jouer

1. Assurez-vous que vous avez installé Symfony sur votre machine.

2. Clonez ce repository.https://gitlab.com/mforen/gamecard.git

3. Ouvrez un terminal et naviguez vers le répertoire du projet.

4. Lancez le serveur Symfony en exécutant `symfony server:start`.

5. Accédez à `http://localhost:8000` dans votre navigateur.

6. Choisissez le mode de jeu en cliquant sur le lien "Play with Graphics".

   - En mode graphique, les mains triées et non triées s'afficheront sur la page.
   - les images represent le trefle,pique,coeur et carreaux

7. avec le test unitaire on teste si:
   - sortHand() trie correctement une main de cartes selon les règles spécifiées
   - drawHand() renvoie une main de cartes avec le bon nombre de cartes
   - si chaque carte est une instance de la classe Card.

<?php
// src/Controller/CardController.php



namespace App\Controller;

use App\Service\CardGame;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
/**
 * @Route("/play/{mode}", name="play")
 */
    public function home()
    {
        return $this->render('card/home.html.twig');
    }

    /**
     * @Route("/play", name="play")
     */
    public function play(Request $request, CardGame $cardGame): Response
    {
        $playMode = $request->query->get('mode');
        $hand = $cardGame->drawHand();
        $sortedHand = $cardGame->sortHand($hand);

        return $this->render('card/play.html.twig', [
            'playMode' => $playMode,
            'hand' => $hand,
            'sortedHand' => $sortedHand,
        ]);
    }
}
<?php
namespace App\Service;

use App\Entity\Card;

class CardGame
{
    private const COLORS = ["Carreaux", "Coeur", "Pique", "Trefle"];
    private const VALUES = ["AS", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi"];

    public function drawHand()
    {
        $hand = [];
        for ($i = 0; $i < 10; $i++) {
            $color = self::COLORS[array_rand(self::COLORS)];
            $value = self::VALUES[array_rand(self::VALUES)];
            $hand[] = new Card($color, $value);
        }
        return $hand;
    }

    public function sortHand($hand)
    {
        usort($hand, function ($card1, $card2) {
            if ($card1->getColor() === $card2->getColor()) {
                return array_search($card1->getValue(), self::VALUES) - array_search($card2->getValue(), self::VALUES);
            }
            return array_search($card1->getColor(), self::COLORS) - array_search($card2->getColor(), self::COLORS);
        });
        return $hand;
    }
}
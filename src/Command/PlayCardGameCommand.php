<?php
// src/Command/PlayCardGameCommand.php

namespace App\Command;

use App\Service\CardGame;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class PlayCardGameCommand extends Command
{
    private $cardGame;

    public function __construct(CardGame $cardGame)
    {
        parent::__construct();

        $this->cardGame = $cardGame;
    }

    protected function configure()
    {
        $this
            ->setName('app:play-card-game')
            ->setDescription('Play a card game.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $output->writeln([
            'Card Game',
            '============',
            '',
        ]);

        $question = new ChoiceQuestion(
            'Choose a play mode',
            ['console', 'graphical'],
            0
        );
        $playMode = $helper->ask($input, $output, $question);

        if ($playMode === 'console') {
            $hand = $this->cardGame->drawHand();
            $sortedHand = $this->cardGame->sortHand($hand);

            $output->writeln('Hand (Non triée):');
            foreach ($hand as $card) {
                $output->writeln($card->getColor() . ' - ' . $card->getValue());
            }

            $output->writeln("\nSorted Hand:");
            foreach ($sortedHand as $card) {
                $output->writeln($card->getColor() . ' - ' . $card->getValue());
            }
        } elseif ($playMode === 'graphical') {
            $output->writeln('You chose graphical mode. Please use the web interface.');
        } else {
            $output->writeln('Invalid play mode.');
        }

        return Command::SUCCESS;
    }
}

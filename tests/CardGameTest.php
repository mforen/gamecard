<?php
// tests/Service/CardGameTest.php
namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\CardGame;
use App\Entity\Card;

class CardGameTest extends TestCase
{
    public function testDrawHand()
    {
        $carte = new CardGame();

        $main = $carte->drawHand();

        $this->assertCount(10, $main);
        foreach ($main as $carte) {
            $this->assertInstanceOf(carte::class, $carte);
        }
    }

    public function testSortHand()
    {
        $carte1 = new Card("Carreaux", "AS");
        $carte2 = new Card("Coeur", "Roi");
        $carte3 = new Card("Pique", "5");

        $unsortedHand = [$carte2, $carte3, $carte1];

        $cardGame = new CardGame();
        $sortedHand = $cardGame->sortHand($unsortedHand);

        $this->assertEquals($carte1, $sortedHand[0]);
        $this->assertEquals($carte2, $sortedHand[1]);
        $this->assertEquals($carte3, $sortedHand[2]);
    }
}